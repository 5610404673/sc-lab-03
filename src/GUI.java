import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class GUI extends JFrame{
	private JFrame frame;
	private JPanel panel1;
	private JPanel panel2;
	private JTextArea text1;
	private JTextField text2;
	private JButton button;
	private JRadioButton radio1;
	private JRadioButton radio2;
	private ButtonGroup bg;
	private ArrayList<String> list1;
	private ArrayList<String> list2;
	
	public GUI(){
		Build();
	}
	
	Controller control = new Controller();
	
	
	public void Build(){
		frame = new JFrame();
		panel1 = new JPanel();
		panel2 = new JPanel();
		text1 = new JTextArea();
		text2 = new JTextField(10);
		button = new JButton("enter");
		radio1 = new JRadioButton("word");
		radio2 = new JRadioButton("n-gram");
		bg = new ButtonGroup();
		
		
		
		setLayout(new GridLayout(0,2));
		//panel1
		add(panel1);
		add(panel2);
		panel1.setLayout(new BorderLayout());
		panel1.add(text1);
		 
		//panel2
		panel2.setLayout(new GridLayout(4,0));
		panel2.add(text2);
		panel2.add(radio1);
		panel2.add(radio2);
		panel2.add(button);
		
		//buttongroup
		bg.add(radio1);
		bg.add(radio2);
		
		
		text1.setText("Please enter your word : ");
		text2.setText("Please enter your n-gram : ");
		button.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				if (radio1.isSelected()){
					list1 = control.getWord(text1.getText());
					String x = "";
					for (int i=0;i<list1.size();i++){
						x += list1.get(i);
					}
					text1.setText("Sentence : "+ x);}
				else {
					list2 = control.getNgram(text1.getText(), Integer.parseInt(text2.getText()));
					String y = "";
					for (int i=0;i<list2.size();i++){
						y += " "+list2.get(i);
					}
					text1.setText("Sentence : "+y);
				}
				}
			
		
	});
	
}
}
